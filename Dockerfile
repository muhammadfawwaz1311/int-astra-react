# pull the base image
FROM node:18.17.1

# set working directory
WORKDIR /app

# copy & install dependencies
COPY package.json .

RUN yarn install

# add app
COPY . .

# set default port
EXPOSE 3000

# build app
CMD [ "yarn", "start" ]