import { fireEvent, render, screen } from '@testing-library/react'
import React from 'react'

import { Provider } from 'react-redux'
import { Home } from '..'
import { store } from '../../state/store'

test('Deposit $ 10', () => {
  const globalStore = store.getState()
  const { count } = globalStore.counterState

  render(
    <Provider store={store}>
      <Home />
    </Provider>
  )

  // Deposit $10
  const handleIncrement = screen.getByTestId('handleIncrement')
  fireEvent.click(handleIncrement)

  const depositCount = store.getState().counterState.count

  expect(depositCount).toBe(count + +'10')

  const balance = screen.getByTestId('count')
  expect(balance).toBeInTheDocument()
})

test('Withdraw $ 10', () => {
  const globalStore = store.getState()
  const { count } = globalStore.counterState

  render(
    <Provider store={store}>
      <Home />
    </Provider>
  )

  // Withdraw $10
  const handleDecrement = screen.getByTestId('handleDecrement')
  fireEvent.click(handleDecrement)

  const withdrawCount = store.getState().counterState.count

  expect(withdrawCount).toBe(count - +'10')

  const balance = screen.getByTestId('count')
  expect(balance).toBeInTheDocument()
})
