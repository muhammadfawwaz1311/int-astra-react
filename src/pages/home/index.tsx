import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { LogoIcons } from 'src/assets/icons'
import { CounterActionType } from 'src/state/actions'
import styles from './style.module.scss'
import type { RootState } from '~/reducers/.'
import type { CounterState } from '~/types/.'

const Home = (): JSX.Element => {
  const { counterState } = useSelector((state: RootState) => state)
  const { count }: CounterState = counterState
  const dispatch = useDispatch()

  const handleDecrement = (): void => {
    if (count > 0) dispatch({ type: CounterActionType.DECREMENT })
  }

  const handleIncrement = (): void => {
    dispatch({ type: CounterActionType.INCREMENT })
  }

  return (
    <div className={styles.container}>
      <img src={LogoIcons} className={styles.images} alt='logo' />
      <div className={styles.balanceWrapper}>
        <span className={styles.balanceDesc}>Total Balance</span>
        <span data-testid='count' className={styles.balanceTitle}>
          $ {count}
        </span>
        <div className={styles.balanceActions}>
          <button
            data-testid='handleIncrement'
            onClick={handleIncrement}
            className={styles.balanceButton}>
            Deposit $ 10
          </button>
          <button
            data-testid='handleDecrement'
            onClick={handleDecrement}
            className={styles.balanceButton}>
            Withdraw $ 10
          </button>
        </div>
      </div>
    </div>
  )
}

export default Home
