enum CounterActionType {
  DECREMENT = 'DECREMENT',
  INCREMENT = 'INCREMENT'
}

export { CounterActionType }
