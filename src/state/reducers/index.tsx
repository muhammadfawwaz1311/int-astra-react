import { combineReducers } from 'redux'

import { counterReducer } from './counter'

const reducers = combineReducers({
  counterState: counterReducer
})

type RootState = ReturnType<typeof reducers>

export { reducers, type RootState }
