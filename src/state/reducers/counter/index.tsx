import { CounterActionType } from 'src/state/actions'
import type { CounterAction, CounterState } from '~/types/.'

const counterState: CounterState = {
  count: 0
}

const counterReducer = (
  state: CounterState = counterState,
  action: CounterAction
): CounterState => {
  switch (action.type) {
    case CounterActionType.DECREMENT:
      return {
        ...state,
        count: state.count - +'10'
      }
    case CounterActionType.INCREMENT:
      return {
        ...state,
        count: state.count + +'10'
      }
    default:
      return state
  }
}

export { counterReducer, counterState }
