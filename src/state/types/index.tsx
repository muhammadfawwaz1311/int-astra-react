import type { CounterAction, CounterState } from './counter'

export type { CounterAction, CounterState }
