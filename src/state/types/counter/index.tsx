interface CounterAction {
  type: string
  payload?: Partial<CounterState>
}

interface CounterState {
  count: number
}

export type { CounterState, CounterAction }
