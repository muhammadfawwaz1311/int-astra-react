import { render, screen } from '@testing-library/react'
import React from 'react'

import { Provider } from 'react-redux'
import App from './App'
import { store } from './state/store'

test('renders app', () => {
  render(
    <Provider store={store}>
      <App />
    </Provider>
  )
  const appElement = screen.getByTestId('app')
  expect(appElement).toBeInTheDocument()
})
