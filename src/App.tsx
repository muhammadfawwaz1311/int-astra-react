import React from 'react'

import './App.scss'
import { Home } from './pages'

const App = (): JSX.Element => {
  return (
    <div data-testid='app' className='app'>
      <Home />
    </div>
  )
}

export default App
